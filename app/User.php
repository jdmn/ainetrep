<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile_photo', 'type', 'admin', 'blocked', 'phone', 'print_evals', 'print_counts', 'department_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSearch($query, $request)
    {
        if ($query != null) {
            $query->where('blocked', 1);
        }

        if (request('ordering') != null) {
            switch (key($request['ordering'])) {
                case 'name':
                    $query->orderBy(key($request['ordering']), $request['ordering']['name']);
                    break;

            }
        }

        return $query;
    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(10), config('app.key'));
    }

    //ACTIVATION

    public function createActivation($user)
    {
        $activation = $this->getActivation($user);

        if (!$activation) {
            return $this->createToken($user);
        }

        return $this->regenerateToken($user);
    }

    private function regenerateToken($user)
    {
        $token = $this->getToken();
        $this->db->table($table)->where('user_id', $user->id)->update([
            'token' => $token,
            'created_at' => new Carbon(),
        ]);

        return $token;
    }

    private function createToken($user)
    {
        $token = $this->getToken();
        $this->db->table($this->table)->insert([
            'user_id' => $user->id,
            'token' => $token,
            'created_at' => new Carbon(),
        ]);

        return $token;
    }

    public function getActivation($user)
    {
        return $this->db->table($this->table)->where('user_id', $user->id)->first();
    }

    public function getActivationByToken($token)
    {
        return $this->db->table($this->table)->where('token', $token)->first();
    }

    public function deleteActivation($token)
    {
        $this->db->table($this->table)->where('token', $token)->delete();
    }

    public function isAdmin()
    {
        return  $this->admin == 1 ? true : false; // or however you determine whether user is admin
    }

    public function requests()
    {
        return $this->hasMany(Request::class, 'owner_id')->where('status', 1);
    }
    public function department()
    {
        return $this->belongsTo(Departments::class, 'department_id');
    }
}
