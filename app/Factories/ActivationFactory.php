<?php

namespace App\Factories;

use App\Repositories\ActivationRepository;
use Illuminate\Mail\Mailer;
//use Illuminate\Mail\Message;
use App\Mail\Activation;
use App\PasswordResets;
use App\Mail\Reset;

class ActivationFactory
{
    protected $activationRepo;
    protected $mailer;
    protected $resendAfter = 24;

    public function __construct(ActivationRepository $activationRepo, Mailer $mailer)
    {
        $this->activationRepo = $activationRepo;
        $this->mailer = $mailer;
    }

    public function sendActivationMail($user)
    {
        $this->activationRepo->createActivation($user);

        $pass_reset = PasswordResets::where('email', request('email'))->first();

        $link = route('register.activation', $pass_reset->token);

        \Mail::to($user)->send(new Activation($user, $link));
    }

    public function sendResetMail($user)
    {
        $pass_reset = PasswordResets::where('email', request('email'))->first();

        $link = route('reset.show', $pass_reset->token);

        \Mail::to($user)->send(new Reset($user, $link));
    }
}
