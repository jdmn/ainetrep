<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Printer;
use App\User;
use App\Mail\FinnishedRequest;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class RequestsController extends Controller
{
    public function create()
    {
        $title = 'PrintIt-Criar pedido';
        $printers = Printer::all();

        return view('requests.create', compact('title', 'printers'));
    }

    public function destroy($id)
    {
        $title = 'PrintIt-Criar pedido';
        $request = \App\Request::where('id', $id)->first();
        $request->delete();
        return redirect(route('requests.index'));
    }

    public function index()
    {
        $requests = null;
        // $ordering=['ordering'=>['name'=>'ASC',
        //           'department_id'=>'ASC']];
        if (auth()->check() && auth()->user()->admin == 0) {
            $title = 'PrintIt-Os meus pedidos';
            $requests = \App\Request::with('printer')->where('owner_id', auth()->user()->id)->Search(request()->all())->select('users.id as userID', 'users.name as userName', 'users.department_id as departmentID', 'requests.status as status', 'requests.quantity as quantity', 'requests.paper_type as paper_type', 'requests.colored as colored', 'requests.printer_id as printerID', 'requests.created_at as created_at', 'requests.id as requestID', 'requests.satisfaction_grade', 'requests.file as file')->paginate(10);
            
        }

        if (auth()->check() && auth()->user()->admin == 1) {
            $title = 'PrintIt-Todos os pedidos';

            $requests = \App\Request::with('user')->Search(request()->all())->select('users.id as userID', 'users.name as userName', 'users.department_id as departmentID', 'requests.status as status', 'requests.quantity as quantity', 'requests.paper_type as paper_type', 'requests.colored as colored', 'requests.printer_id as printerID', 'requests.created_at as created_at', 'requests.id as requestID', 'requests.file as file')->paginate(10);
        }

        if (!auth()->check()) {
            $title = 'PrintIt-Todos os pedidos feitos';
            $requests = \App\Request::with('printer')->with('user')->where('status', '2')->Search(request()->all())->select('users.id as userID', 'users.name as userName', 'users.department_id as departmentID', 'requests.status as status', 'requests.quantity as quantity', 'requests.paper_type as paper_type', 'requests.colored as colored', 'requests.printer_id as printerID', 'requests.created_at as created_at', 'requests.id as requestID')->paginate(10);
        }
        $images = array();
        foreach ($requests as $request) {
            $ext = pathinfo($request->file, PATHINFO_EXTENSION);
            if ($ext == 'gif' || $ext == 'png' || $ext == 'jpg' || $ext == 'tif') {
                $img = Image::make(Storage::get('public/print-jobs/'.$request->userID.'/'.$request->file));
                $img->resize(70, 70, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->stream(); // <-- Key point
                $filename = $request->requestID.'.'.$ext;
                \Storage::disk('public')->put('thumbnails/'.$filename, $img, 'public');
                $images[$request->requestID] = $filename;
            } else {
                $images[$request->requestID] = null;
            }
        }

        foreach (Printer::all() as $printer) {
            $printers[$printer->id]['name'] = $printer->name;
        }

        return view('requests.searchBar', compact('title', 'requests', 'printers', 'images'));
    }

    public function edit($id)
    {
        $title = 'PrintIt-O meu pedido';
        $request = \App\Request::where('id', $id)->first();
        $printers = Printer::all();
        if (auth()->user()->id == $request->owner_id || auth()->user()->isAdmin()) {
            return view('requests.edit', compact('title', 'request', 'printers'));
        }

        return redirect('/');
    }

    public function show($id)
    {
        $title = 'PrintIt-O meu pedido';
        $request = \App\Request::where('id', $id)->first();

        $printers = Printer::all();

        return view('requests.show', compact('title', 'request', 'printers'));
    }

    public function evaluate($id)
    {
        $request = \App\Request::where('id', $id)->first();
        $user = \App\User::where('id', $request->owner_id)->first();
        if (request('satisfaction_grade') != null) {
            $request->satisfaction_grade = request('satisfaction_grade');
            $user->print_evals ++;
            $request->save();
            $user->save();
        }
        return back();
    }

    public function update(Request $request, $id)
    {
        $req = \App\Request::where('id', $id)->first();

        if (request('description') != null) {
            $req->description = request('description');
        }
        if (request('quantity') != null) {
            $req->quantity = request('quantity');
        }

        if ($req->status == 0 && request('concluido') == 1) {
            $req->status = request('status');

            $req->closed_user_id = auth()->user()->id;
            $req->closed_date = Carbon::now(1);
            $req->status = 2;

            $user = User::where('id', $req->owner_id)->first();

            $user->print_counts = $user->print_counts + 1;
            $user->save();
            \Mail::to($user)->send(new FinnishedRequest($user, $req));
        }

        if (request('concluir') != null) {
            if (request('concluir') == 2) {
                $req->printer_id = request('printer_id');
            }
            if (request('concluir') == 1) {
                if (request('refused_reason') == "") {
                    $req->refused_reason = "Não foi adicionado o Motivo de recusa";
                } else {
                    $req->refused_reason = request('refused_reason');
                }
            }
            $req->status = request('concluir');
        }

        //falta adicionar qual impressora

        $req->save();

        return back();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'due_date' => 'date',
            'description' => 'required|max:255',
            'quantity' => 'required|numeric',
            'colored' => 'required|numeric|max:1',
            'stapled' => 'required|numeric|max:1',
            'paper_size' => 'required|numeric',
            'paper_type' => 'required|numeric',
            'printer_id' => 'required|numeric',
            'file' => 'required|mimes:jpeg,png,tif,gif,doc,docx,ods,odt,odp,xls,xlsx,pdf|max:10000',
        ]);
    }

    protected function store(Request $request)
    {
        $validator = $this->validator(request()->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $requests = \App\Request::create([
            'status' => 0,
            'created_at' => Carbon::now(1),
            'due_date' => request('dudate'),
            'description' => request('description'),
            'quantity' => request('quantity'),
            'colored' => request('colored'),
            'stapled' => request('stapled'),
            'paper_size' => request('paper_size'),
            'paper_type' => request('paper_type'),
            'printer_id' => request('printer_id'),
            'owner_id' => auth::user()->id,
            'file' => '',
        ]);

        $this->upload($requests);

        return redirect('/');
    }

    public function upload(\App\Request $requests)
    {

        // getting all of the post data
        $file = array('file' => Input::file('file'));
        // setting up rules
        $rules = array('file' => 'required'); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return redirect()->back()->withErrors($validator);
        } else {
            // checking file is valid.
            if (Input::file('file')->isValid()) {
                $fileContent = Input::file('file');
                $file_path = 'print-jobs/'.auth()->user()->id.'/';
                $filename = Input::file('file')->getClientOriginalName();
                \Storage::disk('public')->putFileAs($file_path, $fileContent, $filename, 'public');

                $requests->file = $filename;
                $requests->save();

                // sending back with message
                //Session::flash('success', 'Upload successfully');

                return back();
            } else {
                // sending back with error message.
                // Session::flash('error', 'uploaded file is not valid');
                return back();
            }
        }
    }
}
