<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;
use Auth;

class UsersController extends Controller
{
    public function show(User $user)
    {
        $title = 'Profile';
        // $user=User::find($id);
        $departaments = Department::all();
        if (auth()->check() && auth()->user()->id == $user->id) {
            return view('user.edit', compact('title', 'user', 'departaments'));
        }

        return view('user.show', compact('title', 'user', 'departaments'));
    }

    public function index()
    {
        $title = 'PrintIt ADMIN Home';
        $blocked;
        if (request() != null && request('blocked') == 1) {
            $users = User::Search(request()->all())->paginate(10);
            $blocked = 1;
        } else {
            $users = User::paginate(10);
            $blocked = null;
        }

        return view('user.index', compact('title', 'users', 'blocked'));
    }

    public function edit(User $user)
    {
        // $user=User::find($id);
        $departaments = Department::all();

        if (Auth::user()->id != $user->id) {
            return view('user.show', compact('title', 'user', 'departaments'));
        }

        return view('user.edit', compact('title', 'user', 'departaments'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$data['id'],
            'phone' => 'max:9',
            'department' => 'required|numeric',
            'presentation' => 'max:255',
            'file' => 'image|mimes:jpeg,png,tif,gif|max:5000',
        ]);
    }

    public function update()
    {
        $validator = $this->validator(request()->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = User::find(request('id'));
        $user->name = request('name');
        $user->email = request('email');
        $user->phone = request('phone');
        $user->department_id = request('department');
        $user->presentation = request('presentation');
        //upload de foto
        if (request('file') != null) {
            $this->upload(request(), $user);
        }
        if(request('password')!=null){
           
            $user->password = bcrypt(request('password'));
        }

        $user->save();

        return back();
    }

    public function upload(Request $requests, User $user)
    {
        $img = Image::make($requests['file']);

        $filename = $requests['file']->getClientOriginalName();

        $extension = File::extension($filename);

        $img->resize(120, 120, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->stream(); // <-- Key point

        //dd();
        \Storage::disk('public')->put('images/'.$user->id.'.'.$extension, $img, 'public');

        $user->profile_photo = $user->id.'.'.$extension;
        $user->save();

        return back();
    }
    
    public function updateAdminOptions()
    {
        $user = User::find(request('id'));
        if (request('type') != null) {
            if (request('type') == 0) {
                $user->blocked = 0;
            }
            if (request('type') == 1) {
                $user->blocked = 1;
            }
            if (request('type') == 2) {
                $user->blocked = 2;
            }
        }

        if (request('typeAdmin') != null) {
            if (request('typeAdmin') == 0) {
                $user->admin = 0;
            }
            if (request('typeAdmin') == 1) {
                $user->admin = 1;
            }
        }
        $user->save();

        return back();
    }
}
