<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;

class IndexController extends Controller
{
    public function show()
    {
        $users = User::Search(request()->all())->orderby('name','asc')->paginate(6, ['*'], 'users');

        //  $departamentos = Department::paginate(6, ['*'], 'departamentos');

        $title = 'PrintIt';

        $blocked = null;

        $allRequestsCount = \App\Request::allCompletedRequestsCount();

        $allCompletedRequestsSum = \App\Request::allCompletedRequestsSum();

        $finishedRequestsToday = \App\Request::finishedRequestsToday();

        if ($allCompletedRequestsSum > 0) {
            $allPrintedPagesBlackWhite = \App\Request::allPrintedPagesBlackWhite();
        } else {
            $allPrintedPagesBlackWhite = 0;
        }

        $allDepartmentsWithCounts = \App\Request::allDepartmentsWithCounts();

        $avgDailyPrintsThisMonth = \App\Request::avgDailyPrintsThisMonth();

        $arrPrintedPagesEachDay = \App\Request::arrPrintedPagesEachDay();

        $arrLastMonthStatistics = \App\Request::arrLastMonthStatistics();

        $lastComments = \App\Comment::lastComments();

        return view('index', compact('users', 'title', 'departamentos', 'allRequestsCount', 'allCompletedRequestsSum', 'finishedRequestsToday', 'totalUsers', 'allPrintedPagesBlackWhite', 'avgDailyPrintsThisMonth', 'arrPrintedPagesEachDay', 'blocked', 'arrLastMonthStatistics', 'allDepartmentsWithCounts', 'lastComments'));
    }
}
