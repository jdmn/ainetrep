<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Printer;
use Illuminate\Support\Facades\Validator;

class PrintersController extends Controller
{
    public function index()
    {
        $printers = Printer::paginate(6);

        $title = 'Lista de Impressoras';

        return view('printers.index', compact('title', 'printers'));
    }

    public function create()
    {
        $title = 'Criar Impressora';

        return view('printers.create', compact('title'));
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validator(request()->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $printer = Printer::where('id', $id)->first();

        $printer->name = request('name');

        $printer->save();

        return redirect('/admin/printers');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',

        ]);
    }

    public function store()
    {
        $validator = $this->validator(request()->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $printer = Printer::create([
            'name' => request('name'),

        ]);

        return redirect('/admin/printers');
    }

    public function edit(Printer $printer)
    {
        // $printer=printers::where('id',$id)->first();

        $title = 'Editar Impressora';

        return view('printers.edit', compact('title', 'printer'));
    }
}
