<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'comment' => 'required|max:255',

        ]);
    }

    public function store()
    {
        $validator = $this->validator(request()->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $comment = Comment::create([
            'comment' => htmlspecialchars(request('comment')),
            'request_id' => request('id'),
            'blocked' => 0,
            'user_id' => auth()->user()->id,
        ]);

        if (request('comment_id') != null) {
            $comment->parent_id = request('comment_id');
        }
        $comment->save();

        return back();
    }

    public function update($id)
    {
        $coment = Comment::where('id', $id)->first();

        if ($coment->blocked != request('block')) {
            $coment->blocked = request('block');
        }

        $coment->save();
        
        return back();
    }

    public function blocked()
    {
        $title = "Comentários bloqueados";

        $comments = Comment::where('blocked', 1)->paginate(10);
        
        return view('comments.blocked', compact('title', 'comments'));
    }
}
