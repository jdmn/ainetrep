<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Factories\ActivationFactory;
use App\PasswordResets;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\ActivationRepository;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    use RegistersUsers;

    public function __construct(ActivationRepository $activationRepository, ActivationFactory $activationFactory)
    {
        $this->activationRepository = $activationRepository;
        $this->activationFactory = $activationFactory;
    }

    public function create()
    {
        $title = 'Resetting Password';

        return view('passwords.edit', compact('title'));
    }

    public function store(Request $request)
    {
        $user = User::where('email', request('email'))->first();

        if ($user->activated == 1) {
            //flashmesage please contact your admin
        }
        if ($user->blocked == 1) {
            //flashmesage please activate your account in your email
        }

        if (PasswordResets::where('email', request('email'))->first() != null) {
            PasswordResets::where('email', '=', $user->email)->delete();
        }

        $this->activationRepository->createToken($user);

        $this->activationFactory->sendResetMail($user);

        return redirect()->home();
    }

    public function show($token)
    {
        $passReset = PasswordResets::where('token', $token)->first();

        $user = User::where('email', $passReset->email)->first();

        if ($user != null) {
            auth()->login($user);

            $title = 'New Password';

            return view('passwords.create', compact('title', 'token'));
        }

        return redirect()->home();
    }

    public function update()
    {
        $user = Auth::user();

        $request = request()->all();

        $validator = $this->validator($request);

        if ($validator->fails()) {
            Auth()->logout($user);

            return back()->withErrors($validator);
        }

        $user->password = bcrypt(request('password'));
        $user->save();

        PasswordResets::where('email', '=', $user->email)->delete();

        return redirect()->home();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|min:6|confirmed',
        ]);
    }
}
