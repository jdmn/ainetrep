<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Department;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Socialite;
use App\Repositories\ActivationRepository;
use App\Factories\ActivationFactory;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $activationFactory;
    protected $activationRepository;

    public function __construct(ActivationFactory $activationFactory, ActivationRepository $activationRepository)
    {
        $this->activationRepository = $activationRepository;
        $this->activationFactory = $activationFactory;
    }

    public function create()
    {
        $title = 'Registration';

        $departments = Department::all();

        return view('registration.create', compact('title', 'departments'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function store(Request $request)
    {
        // $request=request()->all();

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'admin' => 0,
            'blocked' => 0,
            'print_evals' => 0,
            'print_counts' => 0,
            'department_id' => request('department'),
            'activated' =>0,

        ]);

        $id = $user->id;
        $user->profile_url = 'users/'.$id.'/profile';
        $user->save();

        $this->activationRepository->createToken($user);

        $this->activationFactory->sendActivationMail($user);

        return redirect()->home();
    }

    public function activateUser($token)
    {
        $user = $this->activationRepository->activateUser($token);

        if ($user != null) {
            auth()->login($user);

            return redirect('/');
        }

        return redirect()->home();
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();

    }

    /**
     * Obtain the user information from facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {  
        $recived = Socialite::driver('facebook')->stateless()->user();

        //$recived = Socialite::driver('facebook')->stateless()->user();

        if (User::where('email', $recived->getEmail())->first() || !empty(User::where('email', 'email')->first())) {

            $this->facebookLogin($recived);

            return redirect()->home();
        }
        
        $this->facebookRegister($recived);
        
        return redirect()->home();
    }

    public function facebookLogin($recived)
    {
        $user = Socialite::driver('facebook')->userFromToken($token);
 
        $existe = User::where('email', $recived->getEmail())->first();

        if ($existe->activated == 0 || $existe->blocked == 1) {
            return redirect('login')->withErrors([
                'message' => 'Your account is blocked. Please: contact the administrator',
            ]);
        }

        Auth()->login($user);
    }

    public function facebookRegister($recived)
    {
        $password = Hash::make(str_random(8));
        $user = User::create([
            'name' => $recived->getName(),
            'email' => $recived->getEmail(),
            'password' => bcrypt('$password'),
            'admin' => 0,
            'blocked' => 0,
            'print_evals' => 0,
            'print_counts' => 0,
            'department_id' => 1,
            'activated'=>1,
        ]);
        $id = $user->id;
        $user->profile_photo = $id.'.jpg';
        $user->profile_url = 'users/'.$id.'/profile';
        $user->save();

        $this->imgUpdate($recived, $user);

        auth()->login($user);
    }

    public function imgUpdate($recived, $user)
    {
        $img = Image::make($recived->getAvatar());
        $img->resize(120, 120, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->stream(); // <-- Key point
        \Storage::disk('public')->put('images/'.$user->id.'.jpg', $img, 'public');
    }
}
