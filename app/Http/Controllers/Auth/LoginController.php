<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'destroy']);
    }

    public function create()
    {
        $title = 'Login';

        return view('session.create', compact('title'));
    }

    public function store()
    {
        $user = User::where('email', request('email'))->first();

        if ($user == false) {
            return $this->create()->withErrors([
                'message' => 'Utilizador não esta registado',
            ]);
        }

        if ($user->activated == 0) {
            return $this->create()->withErrors([
                'message' => 'Is blocked Please activate in email account',
            ]);
        }

        if ($user->blocked == 1) {
            return $this->create()->withErrors([
                'message' => 'Your account is blocked. Please: contact the administrator',
            ]);
        }

        //attempt to auth

        if (!Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            //if not redirect
            return back()->withErrors([
                'message' => 'please check credentials',
            ]);
        }

        return redirect('/');
        // return view('home',compact('title','user'));
    }

    public function destroy()
    {
        Auth()->logout(true);

        return redirect('/');
    }
}
