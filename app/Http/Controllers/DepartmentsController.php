<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Illuminate\Support\Facades\Validator;

class DepartmentsController extends Controller
{
    public function show($id)
    {
        $department = Department::where('id', $id)->first();

        $allRequestsFromDep = Department::allRequestsFromDep($id);

        $printedRequestsToday = Department::printedRequestsToday();

        $arrPrintedPagesEachDay = Department::arrPrintedPagesEachDay();

        $arrLastMonthStatistics = Department::arrLastMonthStatistics();

        $allPrintedPagesBlackWhite = Department::allPrintedPagesBlackWhite();

        $avgDailyRequestsThisMonth = Department::avgDailyRequestsThisMonth();

        if (auth()->check() && auth()->user()->admin == 1) {
            return $this->edit($id);
        }

        $title = 'Departamento';

        return view('departamento.show', compact('title', 'department', 'allRequestsFromDep', 'printedRequestsToday', 'arrPrintedPagesEachDay', 'arrLastMonthStatistics', 'allPrintedPagesBlackWhite', 'avgDailyRequestsThisMonth'));
    }

    public function index()
    {
        $departamentos = Department::paginate(6);

        $title = 'Departamentos list';

        return view('departamento.index', compact('title', 'departamentos'));
    }

    public function edit($id)
    {
        $departamento = Department::where('id', $id)->first();

        $title = 'Departamento';

        return view('departamento.edit', compact('title', 'departamento'));
    }

    public function update($id)
    {
        $departamento = Department::where('id', request('id'))->first();

        $departamento->name = request('name');

        $departamento->save();

        return redirect('/admin/departamentos');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
        ]);
    }

    public function store()
    {
        $validator = $this->validator(request()->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $departamento = Department::create([
            'name' => request('name'),

        ]);

        return redirect('/admin/departamentos');
    }

    public function create()
    {
        $title = 'Departamento Criar';

        return view('departamento.create', compact('title'));
    }
}
