<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class Activation extends Mailable
{
    public $user;
    public $password_resets;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, String $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.activation')->with(['user' => $this->user, 'token' => $this->token]);
    }
}
