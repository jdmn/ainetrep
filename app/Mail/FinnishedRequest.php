<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Request;

class FinnishedRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $request;
    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Request $request)
    {
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.FinnishedRequest')->with(['user' => $this->user, 'request' => $this->request]);
    }
}
