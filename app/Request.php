<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class Request extends Model
{
    protected $fillable = [
        'owner_id', 'status', 'open_date', 'due_date', 'description', 'quantity', 'colored', 'stapled', 'paper_size', 'paper_type', 'file', 'printer_id', 'closed_date', 'closed_user_id', 'refused_reason', 'satisfaction_grade', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    private static $allRequests;
    private static $arrPrintedPagesEachDay;

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function printer()
    {
        return $this->belongsTo(Printer::class, 'printer_id');
    }

    public function scopeSearch($query, $request)
    {
        if ($query != null) {
            $query->join('users', 'owner_id', '=', 'users.id');

            foreach ($request as $key => $value) {
                if ($key == 'status') {
                    $query->join('printers', 'printer_id', '=', 'printers.id');
                }
                if ($value != null && $key != 'created_atFinal' && $key != 'ordering' && $key != 'page' && $key != 'search') {
                    if ($key == 'created_at' && $request['created_atFinal'] != null) {
                        $query->where('requests.created_at', '>=', request('created_at'))
                            ->where('requests.created_at', '>=', request('created_atFinal'))
                            ->get();
                    } elseif ($key != 'ordering') {
                        $query->where($key, '=', $value);
                    }
                }
            }

            if (isset($request['search'])) {
                $columns = ['users.name', 'users.department_id', 'requests.status', 'requests.quantity', 'requests.paper_type', 'requests.colored', 'printers.name', 'requests.created_at']; //faltam colunas
                $first = true;
                foreach ($columns as $key) {
                    if ($first == true) {
                        $query->orwhere($key, 'like', '%'.$request['search'].'%');
                        $first = false;
                    } else {
                        $query->orwhere($key, 'like', '%'.$request['search'].'%');
                    }
                }
            }
        }

        if (request('ordering') != null) {
            switch (key($request['ordering'])) {
                case 'name':
                    $query->orderBy(key($request['ordering']), $request['ordering']['name']);
                    break;
                case 'nameImp':
                    $query->orderBy('printers.name', $request['ordering']['nameImp']);
                    break;
                case 'created_at':
                    $query->orderBy(key($request['ordering']), $request['ordering']['created_at']);
                    break;
                case 'department_id':
                    $query->orderBy(key($request['ordering']), $request['ordering']['department_id']);
                    break;
                default:
                    $query->orderBy(key($request['ordering']), $request['ordering'][key($request['ordering'])]);
                    break;
            }
        }

        return $query;
    }

    public function coloredToStr()
    {
        if ($this->colored == 1) {
            return 'Cores';
        } else {
            return 'Preto';
        }
    }
    public function statusToStr()
    {
        if ($this->status == 1) {
            return 'Recusado';
        } elseif ($this->status == 0) {
            return 'Por Imprimir';
        } elseif ($this->status == 2) {
            return 'Concluído';
        }
    }

    public function stapledToStr()
    {
        if ($this->stapled == 0) {
            return 'Sim';
        } else {
            return 'Não';
        }
    }

    public function paper_sizeToStr()
    {
        switch ($this->paper_size) {

            case 1:
                return 'A1';
                break;
            case 2:
                return 'A2';
                break;
            case 3:
                return 'A3';
                break;
            case 4:
                return 'A4';
                break;
            case 5:
                return 'A5';
                break;
        }
    }

    public function paper_typeToStr()
    {
        switch ($this->paper_type) {

            case 0:
                return 'Rascunho';
                break;
            case 1:
                return 'Normal';
                break;
            case 2:
                return 'Fotografico';
                break;
        }
    }

    public static function allPaginated()
    {
        return self::where('status', '1')->paginate(6, ['*'], 'requests');
    }

    public static function allCompletedRequestsCount()
    {
        self::$allRequests = self::where('status', 2)->get();

        return self::$allRequests->count();
    }
    public static function allCompletedRequestsSum()
    {
        self::$allRequests = self::where('status', 2)->get();

        return self::$allRequests->sum('quantity');
    }

    public static function finishedRequestsToday()
    {
        return self::whereDate('closed_date', '=', Carbon::parse('today')->toDateTimeString())->where('status', 2)->sum('quantity');
    }

    public static function finishedrequestsThisMonth()
    {
        return self::whereMonth('closed_date', '=', date('m'))->where('status', 2)->sum('quantity');
    }

    public static function allPrintedPagesBlackWhite()
    {
        return round((self::where('colored', '0')->where('status', 2)->sum('quantity') / self::$allRequests->sum('quantity')) * 100, 2);
    }

    public static function allDepartmentsWithCounts()
    {
        $allRequestsWithDept = self::where('status', 2)->join('users', 'users.id', '=', 'owner_id')->join('departments', 'departments.id', '=', 'users.department_id')->select('requests.id as rId', 'departments.id as dId', 'departments.name', 'requests.quantity')->get();
        $allDepartments = Department::all();
        $allDepartmentsWithCounts = new Collection();
        $auxArray = array();
        foreach ($allDepartments as $department) {
            $auxArray["$department->name"] = array('id' => $department->id, 'count' => 0);
        }
        foreach ($allRequestsWithDept as $request) {
            $auxArray["$request->name"]['count'] += $request->quantity;
        }
        foreach ($auxArray as $key => $value) {
            $allDepartmentsWithCounts->add(['depName' => "$key", 'count' => $value['count'], 'id' => $value['id']]);
        }

        // return $allDepartmentsWithCounts->sortBy('depName');

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 6;
        $currentPageSearchResults = $allDepartmentsWithCounts->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $entries = new LengthAwarePaginator($currentPageSearchResults, count($allDepartmentsWithCounts), $perPage);

        //new Paginator($allDepartmentsWithCounts, 3)
        return $entries;
    }
    public static function avgDailyPrintsThisMonth()
    {
        $days = date('d');
        return round(self::finishedRequestsThisMonth() / $days, 1);
    }

    public static function arrPrintedPagesEachDay()
    {
        $today = Carbon::parse('today-30 days');
        $printRequestsLast30DaysColor = self::whereDate('closed_date', '>=', $today->toDateString())->where('status', 2)->where('colored', 1)->get()->groupBy(function ($date) {
            return Carbon::parse($date->closed_date)->format('d/m/Y'); // grouping by years
            //return Carbon::parse($date->created_at)->format('m'); // grouping by months
        })->toArray();

        $printRequestsLast30DaysBlackwhite = self::whereDate('closed_date', '>=', $today->toDateString())->where('status', 2)->where('colored', 0)->get()->groupBy(function ($date) {
            return Carbon::parse($date->closed_date)->format('d/m/Y'); // grouping by years
            //return Carbon::parse($date->created_at)->format('m'); // grouping by months
        }, 'colored')->toArray();

        self::$arrPrintedPagesEachDay = array();

        for ($i = 0; $i < 31; ++$i) {
            $todayddmm = $today->format('d/m');
            $todayddmmyy = $today->format('d/m/Y');
            if (isset($printRequestsLast30DaysBlackwhite[$todayddmmyy])) {
                $count = 0;
                foreach ($printRequestsLast30DaysBlackwhite[$todayddmmyy] as $request) {
                    $count += $request['quantity'];
                }
                self::$arrPrintedPagesEachDay[$todayddmm]['blackWhite'] = $count;
            } else {
                self::$arrPrintedPagesEachDay[$todayddmm]['blackWhite'] = 0;
            }
            if (isset($printRequestsLast30DaysColor[$todayddmmyy])) {
                $count = 0;
                foreach ($printRequestsLast30DaysColor[$todayddmmyy] as $request) {
                    $count += $request['quantity'];
                }
                self::$arrPrintedPagesEachDay[$todayddmm]['color'] = $count;
            } else {
                self::$arrPrintedPagesEachDay[$todayddmm]['color'] = 0;
            }
            $today->addDay();
        }

        return self::$arrPrintedPagesEachDay;
    }
    public static function arrLastMonthStatistics()
    {
        $arrLastMonthStatistics['totalBlackWhite'] = 0;
        $arrLastMonthStatistics['totalColor'] = 0;
        $arrLastMonthStatistics['total'] = 0;
        // $arrPrintedPagesEachDay = Request::arrPrintedPagesEachDay();

        foreach (self::$arrPrintedPagesEachDay as $day) {
            $arrLastMonthStatistics['totalColor'] += $day['color'];
            $arrLastMonthStatistics['totalBlackWhite'] += $day['blackWhite'];
            $arrLastMonthStatistics['total'] = $arrLastMonthStatistics['totalColor'] + $arrLastMonthStatistics['totalBlackWhite'];
        }

        return $arrLastMonthStatistics;
    }
}
