<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Department extends Model
{
    protected $fillable = ['name'];

    private static $id;

    private static $allRequestsFromDep;

    public function user()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'department_id');
    }

    public static function allRequestsFromDep($id)
    {
        self::$id = $id;
        self::$allRequestsFromDep = Request::where('status', 2)->join('users', 'users.id', '=', 'owner_id')->join('departments', 'departments.id', '=', 'users.department_id')->select('requests.id as rId', 'departments.id as dId', 'departments.name', 'requests.closed_date as closed_date', 'requests.quantity as quantity')->where('departments.id', $id);

        return self::$allRequestsFromDep->sum('quantity');
    }

    public static function finishedRequestsThisMonth()
    {
        self::allRequestsFromDep(self::$id);

        return self::$allRequestsFromDep->whereMonth('closed_date', '=', date('m'))->sum('quantity');
    }

    public static function printedRequestsToday()
    {
        $today = Carbon::parse('today');

        return self::$allRequestsFromDep->select('requests.id')->whereDate('closed_date', '=', $today->toDateString())->sum('quantity');
    }

    public static function allPrintedPagesBlackWhite()
    {
        $total = self::allRequestsFromDep(self::$id);
        if ($total ==0) {
            return 0;
        }
        return round((self::$allRequestsFromDep->where('colored', '0')->sum('quantity') / $total) * 100, 2);
    }

    public static function arrPrintedPagesEachDay()
    {
        self::allRequestsFromDep(self::$id);
        $today = Carbon::parse('today-30 days');
        $printRequestsLast30DaysColor = self::$allRequestsFromDep->whereDate('closed_date', '>=', $today->toDateString())->where('colored', 1)->get()->groupBy(function ($date) {
            return Carbon::parse($date->closed_date)->format('d/m/Y'); // grouping by years
            //return Carbon::parse($date->created_at)->format('m'); // grouping by months
        })->toArray();
        self::allRequestsFromDep(self::$id);
        $printRequestsLast30DaysBlackwhite = self::$allRequestsFromDep->whereDate('closed_date', '>=', $today->toDateString())->where('colored', 0)->get()->groupBy(function ($date) {
            return Carbon::parse($date->closed_date)->format('d/m/Y');
        }, 'colored')->toArray();

        $arrPrintedPagesEachDay = array();

        for ($i = 0; $i < 31; ++$i) {
            $todayddmm = $today->format('d/m');
            $todayddmmyy = $today->format('d/m/Y');
            $arrPrintedPagesEachDay[$todayddmm]['blackWhite'] = 0;
            $arrPrintedPagesEachDay[$todayddmm]['color'] = 0;
            if (isset($printRequestsLast30DaysBlackwhite[$todayddmmyy])) {
                foreach ($printRequestsLast30DaysBlackwhite[$todayddmmyy] as $request) {
                    $arrPrintedPagesEachDay[$todayddmm]['blackWhite'] += $request['quantity'];
                }
            }
            if (isset($printRequestsLast30DaysColor[$todayddmmyy])) {
                foreach ($printRequestsLast30DaysColor[$todayddmmyy] as $request) {
                    $arrPrintedPagesEachDay[$todayddmm]['color'] += $request['quantity'];
                }
            }
            $today->addDay();
        }

        return $arrPrintedPagesEachDay;
    }

    public static function avgDailyRequestsThisMonth()
    {
        $days = date('d');

        return round(self::finishedRequestsThisMonth() / $days, 1);
    }

    public static function arrLastMonthStatistics()
    {
        $arrLastMonthStatistics['totalBlackWhite'] = 0;
        $arrLastMonthStatistics['totalColor'] = 0;
        $arrLastMonthStatistics['total'] = 0;
        $arrPrintedPagesEachDay = self::arrPrintedPagesEachDay();

        foreach ($arrPrintedPagesEachDay as $day) {
            $arrLastMonthStatistics['totalColor'] += $day['color'];
            $arrLastMonthStatistics['totalBlackWhite'] += $day['blackWhite'];
            $arrLastMonthStatistics['total'] = $arrLastMonthStatistics['totalColor'] + $arrLastMonthStatistics['totalBlackWhite'];
        }

        return $arrLastMonthStatistics;
    }
}
