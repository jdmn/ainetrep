<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'comment', 'blocked', 'request_id', 'parent_id', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function request()
    {
        return $this->belongsTo(\App\Request::class);
    }

    public function isBlocked()
    {
        return $this->blocked;
    }
    
    public static function lastComments()
    {
        return self::orderBy('created_at', 'desc')->with('user')->take(10)->get()->toArray();
    }
}
