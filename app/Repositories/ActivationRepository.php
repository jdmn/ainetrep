<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\PasswordResets;
use App\User;

class ActivationRepository
{
    public function getActivationByToken($token)
    {
        return PasswordResets::where('token', $token)->first();
    }

    public function deleteActivation($token)
    {
        $delete = PasswordResets::where('token', '=', $token)->delete();
    }

    public function createActivation($user)
    {
        $activation = $user->activated;

        if (!$activation) {
            return $this->createToken($user);
        }
    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(20), config('app.key'));
    }

    public function createToken($user)
    {
        $token = $this->getToken();

        PasswordResets::insert([
            'email' => $user->email,
            'token' => $token,
            'created_at' => new Carbon(),
        ]);

        return $token;
    }

    public function activateUser($token)
    {
        $activation = $this->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::where('email', $activation->email)->first();

        if ($user->activated == 1) {
            return $user = null;
        }
        $user->activated = 1;

        $user->save();

        $this->deleteActivation($token);

        return $user;
    }
}
