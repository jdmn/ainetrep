<div class="row profile" style="width: 20%; margin: 0px; float: left;  padding-left: 10px;  margin-bottom: 50px; " >
    <div class="col-md-3" style="width: 100%">
        <div class="profile-sidebar">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
                <a href="{{route('user.show', Auth::user())}}">
                @if(Auth::user()->profile_photo!=null)
                <img style="  overflow: hidden; width: 70px; height: 70px; border-radius: 50%; margin-bottom: 5px"  src="{{route('avatar.show',Auth::user()->profile_photo)}}" />
                @else
                <img style="  overflow: hidden; width: 70px; height: 70px; border-radius: 50%; margin-bottom: 5px"   src="{{url('avatar.png')}}" />
                @endif
                </a>
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    <h2 style="font-size: 10px;margin: auto"> {{Auth::user()->name}}
                    </h2>
                </div>
                <div class="profile-usertitle-job">
                    @if(Auth::check() && Auth::user()->isAdmin())
                    <p>ADMIN</p>
                    @endif
                </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->       
            <!-- SIDEBAR MENU -->
            <div class="profile-usermenu">
                <ul class="nav">
            @if(Auth::check()&& !Auth::user()->isAdmin())
           
                 
                    <li class="active">
                        <a href="{{route('requests.create') }}">
                        <i class="glyphicon glyphicon-pencil" style="margin-right: 5px">
                        </i>
                        Novo Pedido
                        </a>
                    </li>
                    <li>
                        <a href="{{route('requests.index') }}">
                        <i class="glyphicon glyphicon-th-list" style="margin-right: 5px">
                        </i>
                        Os Meus Pedidos 
                        </a>
                    </li>            
            @endif
             <li class="active">
                        <a href="{{url('users/'.auth()->user()->id.'/edit')}}">   
                        <i class="glyphicon glyphicon-pencil" style="margin-right: 5px">
                        </i>
                        Editar perfil
                        </a>
                    </li>            
            </ul>
            </div>
            @if(Auth::check() && Auth::user()->isAdmin())
            <div class="profile-usermenu">
                <ul class="nav">
                    <li class="active">
                        <a href="{{route('requests.index') }}">
                        <i class="glyphicon glyphicon-search" style="margin-right: 5px">
                        </i>
                        Procurar Pedidos
                        </a>
                    </li>
                    <li>
                        <a href="{{route('users.index') }}">
                        <i class="glyphicon glyphicon-th-list" style="margin-right: 5px">
                        </i>
                        Lista de Utilizadores
                        </a>
                    </li>
                    <li>
                        <a href="{{route('departamentos.index') }}">
                        <i class="glyphicon glyphicon-th-list" style="margin-right: 5px">
                        </i>
                        Lista de Departamentos
                        </a>
                    </li>
                    <li>
                        <a href="{{route('printers.index') }}">
                        <i class="glyphicon glyphicon-th-list" style="margin-right: 5px">
                        </i>
                        Lista de Impressoras
                        </a>
                    </li>
                    <li>
                        <a href="{{route('departamento.create') }}">
                        <i class="glyphicon glyphicon-pencil" style="margin-right: 5px">
                        </i>
                        Inserir Departamentos 
                        </a>
                    </li>
                    <li>
                        <a href="{{route('printer.create') }}">
                        <i class="glyphicon glyphicon-pencil" style="margin-right: 5px">
                        </i>
                        Inserir impressora 
                        </a>
                    </li>
                    <li>
                        <a href="{{route('comments.blocked') }}">
                        <i class="glyphicon glyphicon-th-list" style="margin-right: 5px">
                        </i>
                        Comentários Bloqueados
                        </a>
                    </li>
                </ul>
            </div>
            @endif
        </div>
    </div>
</div >