@extends("master")
@section("content")
<div class="container" style="margin-top:120px;">
    <h1 style="margin-left: 130px;">{{$department->name}}</h1>
</div>
<div  class="container-fluid"  style="padding:0px;width: 58%; margin:auto; "">
    <div class="row">
        <h5 style="text-align: center; margin-right: 50px;">Os últimos 30 dias do departamento {{$department->name}}
        </h5>
        <canvas id="lastMonthChart" width=100% height=30% ></canvas>
        <div class="col-sm-4">
            <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#ff992d;"></label>
            <h6 style="display: inline;">Total: {{$arrLastMonthStatistics['total']}} Impressões</h6>
        </div>
        <div class="col-sm-4">
            <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:rgb(66, 244, 158);"></label>
            <h6 style="display: inline;">Cores: {{$arrLastMonthStatistics['totalColor']}} Impressões</h6>
        </div>
        <div class="col-sm-4">
            <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#007acc;  margin-left: 20px;"></label>
            <h6 style="display: inline; ">Preto e branco: {{$arrLastMonthStatistics['totalBlackWhite']}} Impressões</h6>
        </div>
    </div>
    <!-- script do lineChart -->
    <script type="text/javascript"> 
        $(document).ready(function() {});
        
        $.getScript('{{ URL::asset('js/charts.js') }}',function(){
          
            var data = {
                labels : [
                  @foreach($arrPrintedPagesEachDay as $key => $value)
                  "{{$key}}",
                  @endforeach
        
                ],
                datasets : [
                {
                      label : "Total",
                      fillColor : "rgba(255, 153, 45,0.5)",
                      strokeColor : "rgba(255, 153, 45,1)",
                      pointColor : "rgba(255, 153, 45,1)",
                      pointStrokeColor : "#fff",
                      data : [@foreach($arrPrintedPagesEachDay as $day)
                            "{{$day['color']+$day['blackWhite']}}",
                            @endforeach]
                    },
                    {
                      label : "Cores",
                      fillColor : "rgba(66, 244, 158,0.5)",
                      strokeColor : "rgba(66, 244, 158,1)",
                      pointColor : "rgba(66, 244, 158,1)",
                      pointStrokeColor : "#fff",
                      data : [@foreach($arrPrintedPagesEachDay as $day)
                        "{{$day['color']}}",
                        @endforeach]
                    },
                    {
                      label : "Preto e Branco",
                      fillColor : "rgba(0, 122, 204,0.5)",
                      strokeColor : "rgba(0, 122, 204, 1)",
                      pointColor : "rgba(0, 122, 204,1)",
                      pointStrokeColor : "#fff",
                      data : [@foreach($arrPrintedPagesEachDay as $day)
                        "{{$day['blackWhite']}}",
                        @endforeach]
                    }
                    
                ]
            }
            var options = {
                animation: true,
                responsive: true,
                title: {
                    display: true,
                    text: 'Custom Chart Title'
                },
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 99, 132)'
                    }
                }
            };
            //Get the context of the canvas element we want to select
            var c = $('#lastMonthChart');
            var ct = c.get(0).getContext('2d');
            var ctx = document.getElementById("lastMonthChart").getContext("2d");
            /*************************************************************************/
            myNewChart = new Chart(ctx).Line(data,options);
        })  
    </script>
    <!-- /script do lineChart -->
    <!-- <div class="col-sm-8"> -->
    <!--       <h2>About Company Statistic</h2><br> -->
    <div class="container-fluid">
        <!-- five columns of text below the carousel -->
        <div class="row">
            <div class="col-sm-4" style="width: 25%" >
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6" >
                            <div class="vertical-skills pull-center xs-center" >
                                <ul class="list-inline">
                                    <li><span class="skill" style="height:85%; background:#007acc;">
                                        </span>
                                    </li>
                                </ul>
                                <h4>Nº total de impressões</h4>
                                <h4>{{$allRequestsFromDep}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- </div> -->
            </div>
            <!-- /.col-lg-4 -->
            <!-- <div class="row"> -->
            <div class="col-sm-4" style="width: 25%; ">
                <div class="container-fluid">
                    <canvas id="donutChart" width="150" height="150"></canvas>
                    <!-- script do donut -->
                    <script type="text/javascript"> 
                        $(document).ready(function() {});
                          $.getScript('{{ URL::asset('js/charts.js') }}',function(){
                            var data = [{
                                value: {{$allPrintedPagesBlackWhite}},
                                color: "#007acc"
                                }, {
                                value: {{100-$allPrintedPagesBlackWhite}},
                                color: "#E2EAE9"
                                }]
                            var options = {
                              animation: true,
                              reaponsive: true
                            };
                            //Get the context of the canvas element we want to select
                            var c = $('#donutChart');
                            var ct = c.get(0).getContext('2d');
                            var ctx = document.getElementById("donutChart").getContext("2d");
                            /*************************************************************************/
                            myNewChart = new Chart(ct).Doughnut(data, options);
                        })
                    </script>
                    <!-- /script do donut -->
                    <div class="row" style="text-align:left;">
                        <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#007acc;"></label>
                        <h6 style="display: inline;">Preto e branco: {{$allPrintedPagesBlackWhite}}%</h6>
                    </div>
                    <div class="row" style="text-align:left;">
                        <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#E2EAE9; "></label>
                        <h6 style="display: inline;">Cores: {{100-$allPrintedPagesBlackWhite}}%</h6>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-4 -->
            <!-- <div class="row"> -->
            <div class="col-sm-4" style="width: 25%">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="vertical-skills">
                                <ul class="list-inline">
                                    <li><span class="skill" style="height:98%; background:#007acc;">
                                        </span>
                                    </li>
                                </ul>
                                <h4>Nº impressões hoje</h4>
                                <h4>{{$printedRequestsToday}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- </div> -->
            </div>
            <!-- /.col-lg-4 -->
            <!-- <div class="row"> -->
            <div class="col-sm-4" style="width: 25%">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="vertical-skills">
                                <ul class="list-inline">
                                    <li><span class="skill" style="height:45%; background:#007acc;">
                                        </span>
                                    </li>
                                </ul>
                                <h4>Media diaria mês actual</h4>
                                <h4>{{$avgDailyRequestsThisMonth}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- </div> -->
            </div>
            <!-- /.col-lg-4 -->
            <!-- <br><button class="btn btn-default btn-lg">Get in Touch</button> -->
        </div>
    </div>
</div>
</div>
@endsection