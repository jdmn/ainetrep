@extends('master')
@section('content')
@include("autenticado.partials.aside")
<div style="width: 40%; margin: auto; float:left ; padding: 10px; margin-left: 60px; margin-bottom: 50px;" >
    <form method="post" action='/admin/departamentos/{{$departamento->id}}'>
        {{csrf_field()}}
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{$departamento->id}}" />
        <div class="form-group">
            <label for="name">Nome:
            </label>
            <input type="name" class="form-control" id="name" name="name" value="{{$departamento->name}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" id="password" name="password">Editar
            </button>
        </div>
    </form>
    @include('layouts.errors')
</div>
@endsection