<div class="row slideanim">
    @foreach ($departamentos as $dep)
    <div class="col-sm-4">
        <span class="glyphicon glyphicon-briefcase logo-small" style="color:#007acc;">
        </span>
        <h4>{{$dep->name }}</h4>
        <p>
            <a href="{{route('departamento.show',$dep->id) }}">Ver Profile
            </a>
        </p>
    </div>
    @endforeach
</div>