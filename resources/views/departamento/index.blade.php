@extends('master')
@section('content')
@include("autenticado.partials.aside")
<div id="dept" class="container-fluid text-center">
    <h2 style="margin-left: 30px; margin-top: 25px">Departamentos:
    </h2>
    @if(count($departamentos))
    @include('departamento.partials.departamentos')
    <div style="display: inline;margin: auto ; text-align: center; ">
        @if(!Auth::check())
        <div > {{ $departamentos->links()}}
        </div>
        @endif
    </div>
    @else 
    <h2>No departamentos found
    </h2>
    @endif
</div>
@endsection