@extends("master")
@section("content")
<div class="form-group" style="float:left;padding: 10px; margin-left: 120px">
    @if($user->profile_photo!=null)
    <img style=" margin: auto; overflow: hidden; width: 120px; height: 120px; border-radius: 50%; " id="photo" src="{{route('avatar.show',$user->profile_photo)}}" />
    @else
    <img style="  overflow: hidden; width: 70px; height: 70px; border-radius: 50%; margin-bottom: 5px" src="{{url('avatar.png')}}" />
    @endif
</div>
<div style="width: 35%; margin: auto; float:left ; padding: 10px; margin-left: 60px; margin-bottom: 50px;margin-right: 50px" >
    <div class="form-group">
        <label for="name">Nome:</label>
        <p type="name" class="form-control" id="name" name="name">{{$user->name}}</p>
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <p type="email" class="form-control" id="email" name="email">{{$user->email}}</p>
    </div>
    <div class="form-group">
        <label for="phone">Telefone:</label>
        <p type="phone" class="form-control" id="phone" name="phone">{{$user->phone}}</p>
    </div>
    <div class="form-group">
        <label for="departamento">Departamento:</label>
        <p type="email" class="form-control" id="departamento" name="departamento">{{$user->department_id}}</p>
    </div>
    @if(Auth::check() && Auth::user()->isAdmin())
    <form method="post" action="{{route('users.adminUpdates',$user->id)}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <input type="hidden" name="id" value="{{$user->id}}" />
    @include('user.partials.block')
    @include('user.partials.admin')
          <button type="submit" class="btn btn-primary" id="guardar" name="guardar">Guardar</button>
          </form>
    @endif
</div>
<div style="margin-top:70px">
    <label for="presentation">Apresentação:</label>
    <p type="text" class="container" style="width: 90%; " id="presentation" name="presentation">{{$user->presentation}}</p>
</div>
@endsection