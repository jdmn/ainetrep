@extends('master')
@section('content')
@if(auth()->check())
@include("autenticado.partials.aside")
@endif
<div id="contact" class="col-sm-8" style=" margin:center; padding-top: 50px; width: 100%;background-color: white; float:left; ">
    <h1 style="margin-left: 30px; margin-top: 25px">Contactos:</h1>
    @if(Auth::check() && Auth::user()->isAdmin())
    {{ Form::open(array('url' => '/admin/users', 'method' => 'get','name'=>'mysubmit')) }}
    <div class="form-group">
        <label for="blocked">blocked:</label>
        <select class="form-group" id="blocked" name="blocked" onchange="this.form.submit()">      
        <option value="" @if( $blocked==null) selected="selected" @endif>All</option>
        <option value="1"  @if( $blocked==1) selected="selected" @endif>blocked</option>                      
        </select>
    </div>
    {{ Form::close() }}  
    @endif
    @if(count($users))
    @include('user.partials.users')
    <div style="display: inline;margin: auto ; text-align: center; ">
        <div >  {{ $users->links()}}</div>
    </div>
    @else 
    <h2>No users found</h2>
    @endif
</div>
@endsection