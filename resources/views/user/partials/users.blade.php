<table class="table table-hover" style="width: 80%; margin: auto; margin-bottom: 50px">
    <thead>
        <tr>
            <th>Name <a href="{{ Request::fullUrlWithQuery(['ordering'=>['name'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                <a href="{{ Request::fullUrlWithQuery(['ordering'=>['name'=> 'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
            </th>
            <th>Email</th>
            <th>Telefone</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{$user->name }}</td>
            <td>{{$user->email }}</td>
            <td>{{$user->phone }}</td>
            <td><a href="{{route('user.show',$user) }}">Ver Perfil</a></td>
        </tr>
        @endforeach
</table>