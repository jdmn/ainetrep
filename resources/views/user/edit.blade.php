@extends("master")
@section("content")
<div class="form-group" style="float:left;padding: 10px; margin-left: 120px">
    @if($user->profile_photo!=null)
    <img style=" margin: auto; overflow: hidden; width: 120px; height: 120px; border-radius: 50%; " id="photo" src="{{route('avatar.show',$user->profile_photo)}}" />
    @else
    <img style="  overflow: hidden; width: 70px; height: 70px; border-radius: 50%; margin-bottom: 5px" src="{{url('avatar.png')}}" />
    @endif
</div>
<div style="width: 35%; margin: auto; float:left ; padding: 10px; margin-left: 60px; margin-bottom: 50px;margin-right: 50px" >
<form method="post" action="{{route('user.update',$user->id)}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{ method_field('PUT') }}
    <input type="hidden" name="id" value="{{$user->id}}" />
    <div class="form-group">
        <label for="name">Nome:</label>
        <input type="name" class="form-control" id="name" name="name" value="{{$user->name}}">
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}">
    </div>
    <div class="form-group">
        <label for="phone">Telefone:</label>
        <input type="phone" class="form-control" id="phone" name="phone" value="{{$user->phone}}">
    </div>
    <label for="department">Departamento:</label>
    <select class="form-group" id="department" name="department">  
    @foreach($departaments as $dept)
    <option value="{{$dept->id}}" @if($dept->id==$user->department_id) selected="selected" @endif>{{$dept->name}}</option>
    @endforeach                
    </select>
      <div class="form-group">
        <label for="presentation">Sobre mim:</label>
        <input type="text" class="container" style="width: 90%; " id="presentation" name="presentation" value="{{$user->presentation}}">
    </div>
    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password" >
    </div>
    <div class="form-group">
        <label for="password_confirmation">Password confirmation:</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" >
    </div>
    <script type="text/javascript">$('input[name=department]').on('click', function(e){ 
        e.preventDefault();
        console.log($('input[name=concluido]:checked').val());
         
        });
    </script>
    @if(Auth::check() && Auth::user()->isAdmin())
    @include('user.partials.admin')
    @include('user.partials.block')
    @endif
  
    <div class="form-group">
        <label for="file">Ficheiro:</label>
        {{ Form::file('file')}}
    </div>
    <div class="form-group" >
        <button type="submit" class="btn btn-primary" id="guardar" name="guardar">Guardar</button>
    </div>
</form>
@include('layouts.errors')
@endsection