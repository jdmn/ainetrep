@extends("master")
@section("content")
@include("autenticado.partials.aside")
<div style="width: 30%; margin: auto; float:left ; padding: 10px;  margin-bottom: 50px;" >
    <div class="container" ">
        <h1 ">{{$title}}</h1>
    </div >
    {{ Form::open(array('url' => '/requests', 'method' => 'post', 'files' => true)) }}
    <div class="form-group">
        <label for="description">Descrição:</label>
        <input type="text" class="form-control" id="description" name="description">
    </div>
    <div class="form-group">
        <label for="quantity">Quantidade:</label>
        <input type="number" class="form-control" name="quantity" id="quantity" min="1" >
    </div>
    <select class="form-group" id="colored" name="colored">
        <label for="colored">Tipo de cor:</label>     
        <option value="0">Preto</option>
        <option value="1">Cores</option>
    </select>
    <label for="stapled">Agrafado:</label>
    <select class="form-group" id="stapled" name="stapled">
        <option value="0">Sim</option>
        <option value="1">Não</option>
    </select>
    <label for="paper_size">Tamanho do Papel:</label>
    <select class="form-group" id="paper_size" name="paper_size">
        <option value="0">A5</option>
        <option value="1">A4</option>
        <option value="2">A3</option>
        <option value="3">A2</option>
    </select>
    <label for="paper_type">Tipo de Papel:</label>
    <select class="form-group" id="paper_type" name="paper_type">
        <option value="0">Rascunho</option>
        <option value="1">Normal</option>
        <option value="2">Fotográfico</option>
    </select>
    <label for="printer_id">Impressora:</label>
    <select class="form-group" id="printer_id" name="printer_id">
        @foreach($printers as $printer)
        <option value="{{$printer->id}}">{{$printer->name}}</option>
        @endforeach                
    </select>
    <div class="form-group">
        <label for="file">Ficheiro:</label>
        {{ Form::file('file')}}
    </div>
    <div class="form-group">
        <input class="form-control" id="dudate" name="dudate" placeholder="MM/DD/YYYY" type="text"/>
    </div>
    <!-- Extra JavaScript/CSS added manually in "Settings" tab -->
    <!-- Include jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script>
        $(document).ready(function(){
          var date_input=$('input[name="dudate"]'); //our date input has the name "date"
          var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
          date_input.datepicker({
            format: 'yyyy-mm-dd 23:00:00',
            container: container,
            todayHighlight: true,
            autoclose: true,
          })
        })
    </script>
    <div class="form-group">
        {{ Form::submit() }}
    </div>
    {{ Form::close() }}
</div>
@include('layouts.errors')
@endsection