@extends('master')
@section('content')
@if(auth()->check())
@include("autenticado.partials.aside")
@endif
<div id="requests" class="col-sm-8" style=" margin:center; padding-top: 50px; width: 100%;background-color: white; float:left; ">
    <h1 style="margin-left: 30px; margin-top: 25px">Requests:</h1>
    @if(count($requests))
    @include('requests.partials.list')
    <div style="display: inline;margin: auto ; text-align: center; ">
        <div > {{ $requests->appends(['users' => $users->currentPage(),'departamentos' => $departamentos->currentPage()])->links()}} </div>
    </div>
    @else 
    <h2>No requests found</h2>
    @endif
</div>
@endsection