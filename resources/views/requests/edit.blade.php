@extends("master")
@section("content")
<div style="width: 30%; margin: auto; float:left ; padding: 10px; margin-left: 60px; margin-bottom: 50px;" >
    @if(Auth::user()->id == $request->owner_id && $request->status == 0)
        <form method="post" action="{{route('requests.destroy', $request->id)}}" style="margin-left: 10px;margin-right: 10px">
                    {{csrf_field()}}
            <button type="submit" class="btn btn-danger" id="password" name="password">Eliminar Pedido</button>
        </form>
    @endif
    @if(Auth::user()->isAdmin() && $request->satisfaction_grade != null)
    <div class="form-group">
        <label for="userEval">Avaliação do Pedido:</label>
        <input type="text" class="form-control" value="{{$request->satisfaction_grade}}" id="userEval" name="userEval">
    </div>
    @endif
    @if($request->satisfaction_grade == null && auth()->user()->id == $request->owner_id && $request->status == 2)
    <div class="row">
        <div class="form-group">
            <h1>Avaliação do pedido</h1>
            <form method="post" action="{{route('requests.evaluate', $request->id)}}" style="margin-left: 10px;margin-right: 10px">
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <input type="hidden" name="id" value="{{$request->id}}" />
                <fieldset class="rating" style="margin-right: 20px">
                    <input type="radio" id="star3" name="satisfaction_grade" value="3" /><label class = "full" for="star3" title="Bom - 3 estrelas"></label>
                    <input type="radio" id="star2" name="satisfaction_grade" value="2" /><label class = "full" for="star2" title="Satisfatório - 2 estrelas"></label>
                    <input type="radio" id="star1" name="satisfaction_grade" value="1" /><label class = "full" for="star1" title="Mau - 1 estrela"></label>
                </fieldset>
                <button type="submit" class="btn btn-primary" id="password" name="password">Submeter Avaliação</button>
            </form>
        </div>
    </div>
    @endif
    @if(Auth::user()->id == $request->owner_id && $request->status == 0)
    <form method="post" action="{{route('requests.update', $request->id)}}" style="margin-left: 10px; margin-right: 10px; ">
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <input type="hidden" name="id" value="{{$request->id}}" />
    @endif
    <div class="form-group">
        <label for="description">Descrição:</label>
        <input type="text" class="form-control" value="{{$request->description}}" id="description" name="description">
    </div>
    <div class="form-group">
        <label for="quantity">Quantidade:</label>
        <input type="number" class="form-control" value="{{$request->quantity}}" name="quantity" id="quantity" min="1" max="999">
    </div>
    <label for="colored">Cor:</label>
    <select class="form-group" id="colored" name="colored">
        <option value="0">Preto</option>
        <option value="1">Cores</option>
    </select>
    <label for="stapled">Agrafado:</label>
    <select class="form-group" id="stapled" name="stapled">
        <option value="0">Sim</option>
        <option value="1">Não</option>
    </select>
    <label for="paper_size">Tamanho do Papel:</label>
    <select class="form-group" id="paper_size" name="paper_size">
        <option value="0">A5</option>
        <option value="1">A4</option>
        <option value="2">A3</option>
        <option value="3">A2</option>
    </select>
    <label for="paper_type">Tipo de Papel:</label>
    <select class="form-group" id="paper_type" name="paper_type">
        <option value="0">Rascunho</option>
        <option value="1">Normal</option>
        <option value="2">Fotográfico</option>
    </select>
    <div class="form-group">
        <label for="file">Ficheiro:</label>
        {{ Form::file('file')}}
    </div>
    <div class="form-group">
        <input class="form-control" id="dudate" name="dudate" placeholder="MM/DD/YYYY" type="text"/>
    </div>
    <!-- Extra JavaScript/CSS added manually in "Settings" tab -->
    <!-- Include jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script>
        $(document).ready(function(){
            var date_input=$('input[name="dudate"]'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            date_input.datepicker({
                format: 'yyyy-mm-dd 23:00:00',
                container: container,
                todayHighlight: true,
                autoclose: true,
            })
        })
    </script>
    @if(Auth::user()->isAdmin())
    @include('requests.partials.concluir')
    @endif
    @if(Auth::user()->id == $request->owner_id && $request->status == 0)
        <button type="submit" class="btn btn-primary" id="password" name="password">Editar Pedido</button>
    </form>
    @endif
</div>
<div class="comments" style="width: 50%;float:right; margin-right: 60px; margin-bottom: 50px;">
    <h1>Comentários:</h1>
    <ul class="list-group" >
        @if(count($request->comments))
        @foreach($request->comments as $comment)
        @if($comment->parent_id==null && (auth()->user()->isAdmin() || $comment->isBlocked()==0))
        <li class="list-group-item" >
            @if($comment->user->profile_photo!=null)
            <img style=" margin: auto; overflow: hidden; width: 50px; height: 50px; border-radius: 50%; " id="photo" src="{{route('avatar.show',$comment->user->profile_photo)}}" />
            @else
            <img style="  overflow: hidden; width: 50px; height: 50px; border-radius: 50%; margin-bottom: 5px" src="{{url('avatar.png')}}" />
            @endif
            <strong>
                <h4> {{$comment->user->name}}</h4>
            </strong>
            <strong>
                <p> {{$comment->comment}}</p>
            </strong>
            <h5> {{$comment->created_at}}</h5>
            @include("requests.partials.commentBlock")
            <div class="btn-group">
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                Responder
                <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" style="width: 200px">
                    <div class="card" >
                        <div class="card-block">
                            <form method="post" action='/comment' style="margin-left: 10px;margin-right: 10px">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{{$request->id}}" />
                                <input type="hidden" name="comment_id" value="{{$comment->id}}" />
                                <div class="form-group">
                                    <label for="comment">Comentar:</label>
                                    <input type="name" class="form-control" id="comment" name="comment" required>
                                </div>
                                <div class="form-group" >
                                    <button type="submit" class="btn btn-primary" id="resposta" name="resposta">Responder</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </ul>
            </div>
        </li>
        @endif
        @foreach($request->comments as $commentaux)
        @if($commentaux->parent_id==$comment->id && (auth()->user()->isAdmin() || $comment->isBlocked()==0))
        <li class="list-group-item" style="margin: 20px; background-color: #edf4f9;   border-radius: 5px;" >
            @if($commentaux->user->profile_photo!=null)
            <img style=" margin: auto; overflow: hidden; width: 50px; height: 50px; border-radius: 50%; " id="photo" src="{{route('avatar.show',$commentaux->user->profile_photo)}}" />
            @else
            <img style="  overflow: hidden; width: 50px; height: 50px; border-radius: 50%; margin-bottom: 5px" src="{{url('avatar.png')}}" />
            @endif
            <strong>
                <h4> {{$commentaux->user->name}}</h4>
            </strong>
            <strong>
                <p> {{$commentaux->comment}}</p>
            </strong>
            <h5> {{$commentaux->created_at}}</h5>
            @include("requests.partials.commentRespostas")
        </li>
        <!--teste 3-->
        @endif
        @endforeach 
        @endforeach
        @else
        <strong>
            <h2>Sem Comentários!</h2>
        </strong>
        @endif
    </ul>
    <!-- Add Coments -->
    <div class="card" style="margin: 20px">
        <div class="card-block">
            <form method="post" action='/comment' style="margin-left: 30px;margin-right: 30px">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$request->id}}" />
                <div class="form-group">
                    <label for="comment">Comentar:</label>
                    <input type="name" class="form-control" id="comment" name="comment" required>
                </div>
                <div class="form-group" >
                    <button type="submit" class="btn btn-primary" id="password" name="password">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@include('layouts.errors')
@endsection