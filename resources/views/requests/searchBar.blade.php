@extends("master")
@section("content")
@if(auth()->check())
@include("autenticado.partials.aside")
@endif
<div  class="form-inline" style="width: 90%; margin: auto;  " >
    {{ Form::open(array('url' => '/requests', 'method' => 'get','name'=>'mysubmit')) }}
    <!-- <input type="hidden" name="id" /> -->
    <div style="display: block;">
        <label for="description">Search geral:</label>
        <input type="text" class="form-control" id="search" name="search" value="{{Input::get('search')}}">
    </div>
    <div class="form-group">
        <label for="quantity">Quantidade:</label>
        <input type="number" class="form-control" name="quantity" id="quantity" value="{{Input::get('quantity')}}" >
    </div>
    <div class="form-group" style="float:right;">
        <div class="form-group">
            <label for="status">Estado:</label> 
            <select class="form-group" id="status" name="status">
                <option value="">não definida</option>
                <option value="0">Por imprimir</option>
                <option value="1">Recusado</option>
                <option value="2">Concluído</option>
            </select>
        </div>
        <div class="form-group">
            <label for="colored">Cor:</label> 
            <select class="form-group" id="colored" name="colored">
                <option value="">não definida</option>
                <option value="0">Preto</option>
                <option value="1">Cores</option>
            </select>
        </div>
        <div class="form-group">
            <label for="paper_type">Tipo de Papel:</label>
            <select class="form-group" id="paper_type" name="paper_type">
                <option value="">não definida</option>
                <option value="0">Rascunho</option>
                <option value="1">Normal</option>
                <option value="2">Fotográfico</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="printer_id">Impressora:</label>
        <select class="form-group" id="printer_id" name="printer_id">
            <option value="">não definida</option>
            @foreach($printers as $key => $printer)
            <option value="{{$key}}">{{$printer['name']}}</option>
            @endforeach                
        </select>
    </div>
    <div >
        <label for="created_at">Data inicial:</label>
        <input class="form-control" id="created_at" name="created_at" placeholder="YYYY-MM-DD" type="date"/>
        <label for="created_atFinal">Data final:</label>        
        <input class="form-control" id="created_atFinal" name="created_atFinal" placeholder="YYYY-MM-DD" type="date"/>
    </div>
    <!-- Extra JavaScript/CSS added manually in "Settings" tab -->
    <!-- Include jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script>
        $(document).ready(function(){
          var date_input=$('input[name="created_at","created_atFinal"]'); //our date input has the name "date"
          var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
          date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
          })
        })
    </script>
    {{ Form::submit() }}
    {{ Form::close() }}  
    <script type="text/javascript">$('input[name=mysubmit]').on('click', function(e){ 
        e.preventDefault();
        console.log($('input[name=typeAdmin]:checked').val());
    </script>
</div>
<div class="col-sm-8" style=" margin:auto;width: 100%;  background-color: white; margin-bottom: 50px ">
    <h1 style="margin-left: 10px;">Resultados da Procura:</h1>
    @if(count($requests))
    <table class="table table-striped" >
        <thead>
            <tr>
                <th style="text-align: center;width: 15%">Utilizador  <a href="{{ Request::fullUrlWithQuery(['ordering'=>['name'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>['name'=> 'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th>
                <th style="text-align: center;width: 14%">Departamento <a href="{{ Request::fullUrlWithQuery(['ordering'=>['department_id'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>[ 'department_id'=>  'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th>
                <th style="text-align: center;width: 9%">Estado <a href="{{ Request::fullUrlWithQuery(['ordering'=>['status'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>[ 'status'=>  'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th>
                <th style="text-align: center;width: 12%">Quantidade <a href="{{ Request::fullUrlWithQuery(['ordering'=>['quantity'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>[ 'quantity'=>  'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th>
                <th style="text-align: center;width: 12%">Tipo papel <a href="{{ Request::fullUrlWithQuery(['ordering'=>['paper_type'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>[ 'paper_type'=>  'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th>
                <th style="text-align: center;width: 7%">Cor <a href="{{ Request::fullUrlWithQuery(['ordering'=>['colour'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>[ 'colour'=>  'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th>
                <th style="text-align: center;width: 13%">Impressora <a href="{{ Request::fullUrlWithQuery(['ordering'=>['nameImp'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>[ 'nameImp'=>  'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th>
                <th style="text-align: center;width: 13%">Data Pedido <a href="{{ Request::fullUrlWithQuery(['ordering'=>['created_at'=> 'DESC']]) }}"><i class='fa fa-chevron-down'></i></a>
                    <a href="{{ Request::fullUrlWithQuery(['ordering'=>[ 'created_at'=>  'ASC']]) }}"><i class='fa fa-chevron-up'></i></a>
                </th >
                @if(auth()->check())  
                <th style="text-align: center;width: 8%">Ver Pedido</th>
                <th style="text-align: center;width: 8%">Thumbnail</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($requests as $request)
            <tr>
                <td style="text-align: left;">{{$request->userName}}</td>
                <td style="text-align: center">{{$request->departmentID }}</td>
                <td style="text-align: center">{{$request->statusToStr() }}</td>
                <td style="text-align: center">{{$request->quantity }}</td>
                <td style="text-align: center">{{$request->paper_typeToStr()}}</td>
                <td style="text-align: center">{{$request->coloredToStr()}}</td>
                <td style="text-align: center">@if($request->status==2) {{$printers[$request->printerID]['name']}} @endif</td>
                <td>{{$request->created_at}}</td>
                @if(auth()->check())
                @if($request->status == 2 && $request->satisfaction_grade==null && $request->owner_id == Auth::user()->id)
                <td style="text-align: center"><a href="{{route('requests.edit',$request->requestID) }}">Avaliar Pedido</a></td>
                @else
                <td style="text-align: center"><a href="{{route('requests.edit',$request->requestID) }}">Ver Pedido</a></td>
                @endif                
                @endif
                <td style="text-align: center">
                @if($images[$request->requestID]!= null)
                    <img style="  overflow: hidden; width: 70px; height: 70px; border-radius: 5%; margin-bottom: 5px"  src="{{route('thumbnail.show',$images[$request->requestID])}}" />
                @endif
                </td>
            </tr>
            @endforeach
    </table>
    <div> 
        {{ $requests->appends(Request::except('query'),Input::except('requests'))->render()}}
    </div>
    @else 
    <h2>No requests found</h2>
    @endif
</div>
@endsection