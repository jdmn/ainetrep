@if(Auth::check() && Auth::user()->isAdmin())
{{ Form::open(array('url' => '/comment/'.$commentaux->id, 'method' => 'put','name'=>'mysubmitcomment2')) }}
{{ method_field('PUT') }}
{{ Form::radio('blocked', '1', $commentaux->blocked==1 , array('block'=>'1','name'=>'block')) }}
{{ Form::label('blocked','blocked')}}&nbsp;&nbsp;&nbsp;&nbsp;
{{ Form::radio('notBlocked', '0', $commentaux->blocked==0 , array('block'=>'0','name'=>'block')) }}
{{ Form::label('notBlocked','notBlocked')}}&nbsp;&nbsp;&nbsp;&nbsp;
{{ Form::submit('Save')}}
{{ Form::close() }}  
<script type="text/javascript">$('input[name=mysubmitcomment2]').on('click', function(e){ 
    e.preventDefault();
    console.log($('input[name=block]:checked').val());
     
    });
</script>
@endif