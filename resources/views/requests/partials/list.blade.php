<table class="table table-hover" style="width: 60%; margin: auto">
    <thead>
        <tr>
            <th>User</th>
            <th>description</th>
            <th>quantity</th>
            <th>colored</th>
            <th>stapled</th>
            <th>paper_size</th>
            <th>paper_type</th>
            <th>printer_id</th>
            <th>file</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($requests as $request)
        <tr>
            <td>{{$request->user->name}}</td>
            <td>{{$request->description }}</td>
            <td>{{$request->quantity }}</td>
            <td>{{$request->coloredToStr()}}</td>
            <td>{{$request->stapledToStr()}} </td>
            <td>{{$request->paper_sizeToStr()}}</td>
            <td>{{$request->paper_typeToStr()}}</td>
            <td>{{$request->printer}}</td>
            <td>{{$request->file }}</td>
            <td><a href="{{url('/requests/'.$request->id.'/edit')}}">Ver Pedido</a></td>
        </tr>
        @endforeach
</table>