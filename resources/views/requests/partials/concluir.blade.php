<form method="post" action="{{route('requests.update',$request->id)}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{ method_field('PUT') }}
	<fieldset class="block" style="margin-right: 20px">
		<label for="printer_id">Para concluir escolha a Impressora:</label>
    	<select class="form-group" id="printer_id" name="printer_id">
            @foreach($printers as $printer)
                <option value="{{$printer->id}}">{{$printer->name}}</option>
            @endforeach                
        </select>
    </fieldset>
    <fieldset class="block" style="margin-right: 20px">
    <div class="form-group">
        <label for="description">Para recusar insira o Motivo:</label>
        <input type="text" class="form-control" value="" id="description" name="refused_reason">
    </div>
    </fieldset>
	<fieldset class="block" style="margin-right: 20px">
		<input type="radio" id="status" name="concluir" value="0" @if($request->status==0) checked @endif/><label  for="none" title="Por Imprimir">Por Imprimir</label>
	    <input type="radio" id="status" name="concluir" value="1" @if($request->status==1) checked @endif/><label  for="recusar" title="Recusar">Recusar</label>
	    <input type="radio" id="status" name="concluir" value="2" @if($request->status==2) checked @endif/><label  for="concluir" title="Concluir" style="margin-right: 10px">Concluir</label>
	    <button type="submit" class="btn btn-primary" id="password" name="password">Guardar</button>
	</fieldset>

</form>