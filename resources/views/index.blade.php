@extends("master")
@section("content")
@if(auth()->check())
@include("autenticado.partials.aside")
@endif
<h5 style="text-align: right; margin-right: 50px;">Já imprimimos {{$allCompletedRequestsSum}} cópias!</h5>
<!-- Container (About Section) -->
<div  class="container-fluid" style="padding:0px;width: 57%; margin:auto; ">
    <div class="row" >
        <h5 style="text-align: center; margin-right: 50px;">Os últimos 30 dias da PrinTiT
        </h5>
        <canvas id="lastMonthChart" width=100% height=30% ></canvas>
        <div class="col-sm-4">
            <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#ff992d;"></label>
            <h6 style="display: inline;">Total: {{$arrLastMonthStatistics['total']}} Impressões</h6>
        </div>
        <div class="col-sm-4">
            <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:rgb(66, 244, 158);"></label>
            <h6 style="display: inline;">Cores: {{$arrLastMonthStatistics['totalColor']}} Impressões</h6>
        </div>
        <div class="col-sm-4">
            <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#007acc;  margin-left: 20px;"></label>
            <h6 style="display: inline; ">Preto e branco: {{$arrLastMonthStatistics['totalBlackWhite']}} Impressões</h6>
        </div>
    </div>
    <!-- script do lineChart -->
    <script type="text/javascript"> 
        $(document).ready(function() {});
        
        $.getScript('{{ URL::asset('js/charts.js') }}',function(){
          
            var data = {
                labels : [
                  @foreach($arrPrintedPagesEachDay as $key => $value)
                  "{{$key}}",
                  @endforeach
        
                ],
                datasets : [
                {
                      label : "Total",
                      fillColor : "rgba(255, 153, 45,0.5)",
                      strokeColor : "rgba(255, 153, 45,1)",
                      pointColor : "rgba(255, 153, 45,1)",
                      pointStrokeColor : "#fff",
                      data : [@foreach($arrPrintedPagesEachDay as $day)
                            "{{$day['color']+$day['blackWhite']}}",
                            @endforeach]
                    },
                    {
                      label : "Cores",
                      fillColor : "rgba(66, 244, 158,0.5)",
                      strokeColor : "rgba(66, 244, 158,1)",
                      pointColor : "rgba(66, 244, 158,1)",
                      pointStrokeColor : "#fff",
                      data : [@foreach($arrPrintedPagesEachDay as $day)
                        "{{$day['color']}}",
                        @endforeach]
                    },
                    {
                      label : "Preto e Branco",
                      fillColor : "rgba(0, 122, 204,0.5)",
                      strokeColor : "rgba(0, 122, 204, 1)",
                      pointColor : "rgba(0, 122, 204,1)",
                      pointStrokeColor : "#fff",
                      data : [@foreach($arrPrintedPagesEachDay as $day)
                        "{{$day['blackWhite']}}",
                        @endforeach]
                    }
                    
                ]
            }
            var options = {
                animation: true,
                responsive: true,
                title: {
                    display: true,
                    text: 'Custom Chart Title'
                },
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 99, 132)'
                    }
                }
            };
            //Get the context of the canvas element we want to select
            var c = $('#lastMonthChart');
            var ct = c.get(0).getContext('2d');
            var ctx = document.getElementById("lastMonthChart").getContext("2d");
            /*************************************************************************/
            myNewChart = new Chart(ctx).Line(data,options);
        })  
    </script>
    <!-- /script do lineChart -->
    <div class="col-sm-4" style="width: 25%;" >
        <div class="container-fluid" style="text-align: center;">
            <div class="vertical-skills pull-center xs-center">
                <ul class="list-inline">
                    <li><span class="skill" style="height:85%; background:#007acc;"></span></li>
                </ul>
                <h5>Nº total de Pedidos concluídos</h5>
                <h5>{{$allRequestsCount}}</h5>
            </div>
        </div>
    </div>
    <div class="col-sm-4" style="width: 25%; ">
        <div class="container-fluid">
            <canvas id="donutChart" width="150" height="150"></canvas>
            <!-- script do donut -->
            <script type="text/javascript"> 
                $(document).ready(function() {});
                  $.getScript('{{ URL::asset('js/charts.js') }}',function(){
                    var data = [{
                        value: {{$allPrintedPagesBlackWhite}},
                        color: "#007acc"
                        }, {
                        value: {{100-$allPrintedPagesBlackWhite}},
                        color: "#E2EAE9"
                        }]
                    var options = {
                      animation: true,
                      reaponsive: true
                    };
                    //Get the context of the canvas element we want to select
                    var c = $('#donutChart');
                    var ct = c.get(0).getContext('2d');
                    var ctx = document.getElementById("donutChart").getContext("2d");
                    /*************************************************************************/
                    myNewChart = new Chart(ct).Doughnut(data, options);
                })
            </script>
            <!-- /script do donut -->
            <div class="row" style="text-align:left;">
                <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#007acc;"></label>
                <h6 style="display: inline;">Preto e branco: {{$allPrintedPagesBlackWhite}}%</h6>
            </div>
            <div class="row" style="text-align:left;">
                <label style="width: 10px;height: 10px; margin: 10px 10px 0px 10px; background-color:#E2EAE9; "></label>
                <h6 style="display: inline;">Cores: {{100-$allPrintedPagesBlackWhite}}%</h6>
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <!-- <div class="row"> -->
    <div class="col-sm-4" style="width: 25%">
        <div class="container-fluid">
            <div class="vertical-skills pull-center xs-center">
                <ul class="list-inline">
                    <li><span class="skill" style="height:98%; background:#007acc;">
                        </span>
                    </li>
                </ul>
                <h5>Impressões Concluídas Hoje</h5>
                <h5>{{$finishedRequestsToday}}</h5>
            </div>
        </div>
        <!-- </div> -->
    </div>
    <!-- /.col-lg-4 -->
    <!-- <div class="row"> -->
    <div class="col-sm-4" style="width: 25%;" >
        <div class="container-fluid">
            <div class="vertical-skills pull-center xs-center">
                <ul class="list-inline">
                    <li><span class="skill" style="height:45%; background:#007acc;">
                        </span>
                    </li>
                </ul>
                <h5>Media diária de Impressões no mês actual</h5>
                <h5>{{$avgDailyPrintsThisMonth}}</h5>
            </div>
        </div>
        <!-- </div> -->
    </div>
    <!-- /.col-lg-4 -->
</div>
</div>
<div class="row" id="dept" style="margin:20px 100px; width: 80%; margin: auto; padding-bottom: 50px">
    <div class="nb-card" style="border: 1px solid #007acc; padding: 20px;">
        <div class="card-header">
            <h2>Impressões concluídas por departamento</h2>
        </div>
        <div class="card-body" style="text-align: left;">
            @foreach(array_chunk($allDepartmentsWithCounts->toArray()['data'], 2) as $chunk)
            <div class="row">
                @foreach($chunk as $add)
                <div class="col-md-6">
                    <h4><a href="{{route('departamento.show',$add['id']) }}">{{$add['depName']}}</a></h4>
                    <span class="float-xs-right text-muted progress-info" style="text-align: right;"></span>
                    </h4>
                    <div class="progress" style="border-radius: 0px;">
                        <div class="progress-bar" role="progressbar" style="width: @if($allCompletedRequestsSum>0)
                            {{ ($add['count']/$allCompletedRequestsSum)*100}}%
                            @else 0% 
                            @endif; text-align: left; border-radius: 0px; color: black;" aria-valuenow="{{$add['count']}}" aria-valuemin="0" aria-valuemax="{{$allCompletedRequestsSum}}">
                            {{$add['count']}}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
        <div style="display: inline;margin: auto ; text-align: center; ">
            <div > {{ $allDepartmentsWithCounts->appends(['users' => $users->currentPage()])->links()}}</div>
        </div>
    </div>
</div>
<div class="container-fluid bg-grey" style="background-color: #d9e6f2;">
    <div class="row" style="width: 85%; margin: auto;">
        <div id="contact" class="col-sm-8" style=" margin:center; padding-top: 50px; width: 100%;background-color: white; float:left; ">
            <h1 style="margin-left: 30px; margin-top: 25px">Contactos:</h1>
            @if(count($users))
            @include('user.partials.users')
            <div style="display: inline;margin: auto ; text-align: center; ">
                <div > {{ $users->appends(Input::except('allDepartmentsWithCounts'))->links()}}</div>
            </div>
        </div>
        @else 
        <h2>No users found</h2>
        @endif
    </div>
</div>
</div>
<!-- Container (Portfolio Section) -->
<div id="portfolio" class="container-fluid text-center bg-grey">
<h2>Últimos Comentários</h2>
<div id="myCarousel"  class="carousel slide text-center" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators"  >
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
        <li data-target="#myCarousel" data-slide-to="6"></li>
        <li data-target="#myCarousel" data-slide-to="7"></li>
        <li data-target="#myCarousel" data-slide-to="8"></li>
        <li data-target="#myCarousel" data-slide-to="9"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div  class="carousel-inner" role="listbox">
        @for ($i=0; $i<10; $i++)
        @if($i==0)
        <div class="item active">
            @else
            <div class="item">
                @endif
                <h4>"{{$lastComments[$i]['comment']}}"<br><span>{{$lastComments[$i]['user']['name']}}</span></h4>
            </div>
            @endfor
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" style="color:#007acc" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" style="color:#007acc" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Seguinte</span>
        </a>
    </div>
</div>
@endsection