<footer class=" navbar-default navbar-fixed-bottom" style="background-color:  #e2e6e9; height: 30px; ">
    <p class="pull-right" style=" margin-right: 30px;"><a style="color:black; font-size: 15px" href="#">Back to top</a></p>
    <p style=" margin-left: 30px; ;color:black; font-size: 15px">© 2016 Company, Inc.  <a style="color:black; font-size: 15px" href="#">Terms</a></p>
</footer>
<script>
    $(document).ready(function(){
      // Add smooth scrolling to all links in navbar + footer link
      $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
    
          // Store hash
          var hash = this.hash;
    
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 900, function(){
       
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });
      
      $(window).scroll(function() {
        $(".slideanim").each(function(){
          var pos = $(this).offset().top;
    
          var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
              $(this).addClass("slide");
            }
        });
      });
    })
</script>
</body>
</html>