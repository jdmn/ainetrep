<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container" style="width: 100%">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
            </button>
            <span class="icon-bar"><a href="{{ route('home') }}"><img class="img-circle"  src="{{url('favicon_144x144.png')}}" alt="Generic placeholder image" width="51" height="51" style="padding: 5px; margin-left: 0px"></a></span> 
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            @if(!Auth::check())
            <ul class="nav navbar-nav">
                <li><a href="/">HOME</a></li>
                <li><a href="#dept">DEPARTAMENTOS</a></li>
                <li><a href="#contact">CONTACTOS</a></li>
            </ul>
            @endif
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                <li><a href="{{ route('login.create') }}">LOGIN</a></li>
                <li><a href="{{ route('register.create') }}">REGISTER</a></li>
                @else
                <li><a href="{{ url('/')}}">HOME</a></li>
                <li><a  href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();">LOGOUT</a></li>
                <form id="logout-form" action="{{ route('logout')  }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                @endif        
            </ul>
        </div>
    </div>
</nav>