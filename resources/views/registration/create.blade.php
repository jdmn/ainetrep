@extends('master')
@section('content')
<div class="container" style="margin-top: 100px margin-bottom: 50px">
    <div class="col-sm-8" style="width: 40%;margin: auto ">
        <h1>Register</h1>
        <form method="post" action="/register" >
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>
            <div class="form-group">
                <label for="password_confirmation">Password confirmation:</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
            </div>
            <label for="department">Departamento:</label>
            <select class="form-group" id="department" name="department">
                @foreach($departments as $dept)
                <option value="{{$dept->id}}">{{$dept->name}}</option>
                @endforeach                
            </select>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" id="register" name="register">Register</button>
                <a class="btn btn-primary" href="/register/facebook">Facebook Register</a>
            </div>
            @include('layouts.errors')
        </form>
    </div>
</div>
@endsection