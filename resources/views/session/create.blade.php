@extends('master')
@section('content')
<div class="container" style="margin-top: 100px; ">
    <div class="col-sm-8" style="width: 40%;margin: auto ">
        <h1>Login</h1>
        <form method="post" action="/login">
            {{csrf_field()}}
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" id="login" name="login">Login</button>
                <a class="btn btn-primary" href="/register/facebook">Facebook Login</a>
            </div>
        </form>
        <div>
            <a href="/reset" class="btn btn-danger"  role="button" aria-expanded="false">Reset Password </a>
        </div>
    </div>
    @include('layouts.errors')
</div>
@endsection