@extends('master')
@section('content')
@if(auth()->check())
@include("autenticado.partials.aside")
@endif
<div id="comments" class="col-sm-8" style=" margin:center; padding-top: 50px; width: 100% margin-bottom: 50px;background-color: white; float:left; ">
    <h1 style="margin-left: 30px; margin-top: 25px">Comentários bloqueados:</h1>
    @if(count($comments))
    <div style="display: inline;margin: auto ; text-align: center; ">
    <table class="table table-hover" style="width: 60%; margin: auto">
    <thead>
        <tr>
            <th>Comment</th>
            <th>Id do pedido</th>
            <th>Data da criação</th>
            <th>Ver pedido</th>
            <th>Desbloquear comentário</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($comments as $comment)
        <tr>
            <td>{{$comment->comment}}</td>
            <td>{{$comment->request_id}}</td>
            <td>{{$comment->created_at}}</td>
            <td><a href="{{route('requests.show', $comment->request_id)}}">Ver Pedido</a></td>
            <td><form method="post" action="{{route('comment.update', $comment->id)}}" style="margin-left: 30px;margin-right: 30px">
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <input type="hidden" name="block" value="0" />
                <button type="submit" class="btn btn-primary" id="password" name="password">Desbloquear</button>
                </form>
            </td>
        </tr>
        @endforeach
</table>
        <div > {{ $comments->links()}} </div>
    </div>
    @else 
    <h2>Não existem comentários bloqueados!</h2>
    @endif
</div>
@endsection