@extends('master')
@section('content')
@include("autenticado.partials.aside")
<div id="contact" class="col-sm-8" style=" margin:auto; padding-top: 50px; width: 75%;background-color: white ">
    <h1 style="margin-left: 30px; margin-top: 25px">Printers:</h1>
    @if(count($printers))
    <table class="table table-hover" style="width: 60%; margin: auto">
        <thead>
            <tr>
                <th>Nome</th>
                <th>view Printer</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($printers as $printer)
            <tr>
                <td>{{$printer->name }}</td>
                <td><a href="{{route('printer.edit', $printer)}}">Ver Printer</a></td>
            </tr>
            @endforeach
    </table>
    <div style="display: inline;margin: auto ; text-align: center; ">
        <div > {{ $printers->links() }}</div>
    </div>
    @else 
    <h2>No requests found</h2>
    @endif
</div>
@endsection