@extends('master')
@section('content')
@include("autenticado.partials.aside")
<div style="width: 40%; margin: auto; float:left ; padding: 10px; margin-left: 60px; margin-bottom: 50px;" >
    <form method="post" action='/admin/printers/{{$printer->id}}'>
        {{csrf_field()}}
        {{ method_field('PUT') }}
        <!-- <input type="hidden" name="id" value="{{$printer->id}}" /> -->
        <div class="form-group">
            <label for="name">Nome:</label>
            <input type="name" class="form-control" id="name" name="name" value="{{$printer->name}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" id="password" name="printer">edit</button>
        </div>
    </form>
    @include('layouts.errors')
</div>
@endsection