<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Pedido Concluído</title>
    </head>
    <body>
        <h1>O seu pedido de impressão foi concluído!</h1>
        <p>Detalhes:</p>
        <p>Utilizador: {{$user->name}}</p>
        <p>Id:{{$request->id}}</p>
        <p>Description:{{$request->description}}</p>
        <p>Quantity:{{$request->quantity}}</p>
        <p>Created Date:{{$request->created_at}}</p>
        <p>Clique no link abaixo para avaliar o seu pedido, a sua opinião é muito importante para nós!</p>
        <a href="printit.ainet">www.printit.ainet</a>
        <footer >
            <p>© 2017 Instituto Politécnico de Leiria</p>
        </footer>
    </body>
</html>