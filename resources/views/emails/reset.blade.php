<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Reset Password</title>
    </head>
    <body>
        <!-- <img class="img-circle"  src="{{url('favicon_144x144.png')}}" alt="Generic placeholder image" width="70" height="70" > -->
        <h1>Reset Password EMAIL para {{$user->name}}</h1>
        <p>Se não foi você por favor ignore este e-mail!</p>
        <p>Clique no link abaixo para alterar a sua password</p>
        <a href="{{$token}}">Alterar Password</a>
        <footer >
            <p class="pull-right"><a href="#">Back to top</a></p>
            <p>© 2017 Instituto Politécnico de Leiria<a href="#">Privacy</a> · <a href="#">Terms</a></p>
        </footer>
    </body>
</html>