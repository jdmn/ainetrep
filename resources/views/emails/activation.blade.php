<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Activation</title>
    </head>
    <body>
        <!-- <img class="img-circle"  src="{{url('favicon_144x144.png')}}" alt="Generic placeholder image" width="70" height="70" > -->
        <h1>Activation EMAIL {{$user->name}}</h1>
        <p>Obrigado por se juntar a nós.</p>
        <p>Clique no link abaixo para activar a sua conta!</p>
        <a href="{{$token}}">Activar Conta</a>
        <footer >
            <p class="pull-right"><a href="#">Back to top</a></p>
            <p>© 2017 Instituto Politécnico de Leiria<a href="#">Privacy</a> · <a href="#">Terms</a></p>
        </footer>
    </body>
</html>