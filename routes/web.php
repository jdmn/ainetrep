<?php

Route::group(['middleware' => ['guest']], function () {
    Route::get('/register/create', 'Auth\RegisterController@create')->name('register.create');

    Route::post('/register', 'Auth\RegisterController@store')->name('register.store');

    Route::get('/reset', 'Auth\ResetPasswordController@create')->name('reset.create');

    Route::post('/reset', 'Auth\ResetPasswordController@store')->name('reset.store');

    Route::get('/reset/{token}', 'Auth\ResetPasswordController@show')->name('reset.show');

    Route::get('/login/create', 'Auth\LoginController@create')->name('login.create');

    Route::post('/login', 'Auth\LoginController@store')->name('login.store');

    Route::get('register/activation/{token}', 'Auth\RegisterController@activateUser')->name('register.activation');
});

Route::get('/requests', 'RequestsController@index')->name('requests.index');

Route::get('/', 'indexController@show')->name('home');

Route::get('/departamento/{id}', 'DepartmentsController@show')->name('departamento.show');

Route::get('/users/{user}', 'UsersController@show')->name('user.show');

Route::get('users/avatar/{user}', function ($image) {
    return Image::make(storage_path('app/public/images/'.$image))->response();
})->name('avatar.show');

//facebook
Route::group(['middleware' => ['web']], function () {
    Route::get('/register/facebook', 'Auth\RegisterController@redirectToProvider')->name('register.facebook');

    Route::get('/register/facebook/callback', 'Auth\RegisterController@handleProviderCallback')->name('callback.facebook');
});

Route::group(['middleware' => ['admin']], function () {
    Route::put('/admin/users/{user}', 'UsersController@updateAdminOptions')->name('users.adminUpdates');

    Route::get('/admin/users', 'UsersController@index')->name('users.index');

    Route::get('/comments/blocked', 'CommentsController@blocked')->name('comments.blocked');

    Route::get('/admin/departamentos', 'DepartmentsController@index')->name('departamentos.index');

    Route::get('/admin/departamentos/{departamento}/edit', 'DepartmentsController@edit')->name('departamentos.edit');

    Route::get('/admin/departamentos/create', 'DepartmentsController@create')->name('departamento.create');

    Route::post('/admin/departamentos', 'DepartmentsController@store')->name('departamento.store');

    Route::put('/admin/departamentos/{departamento}', 'DepartmentsController@update')->name('departamento.update');

    Route::get('/admin/printers', 'PrintersController@index')->name('printers.index');

    Route::get('/admin/printers/create', 'PrintersController@create')->name('printer.create');

    Route::post('/admin/printers/store', 'PrintersController@store')->name('printer.store');

    Route::get('/admin/printers/{printer}/edit', 'PrintersController@edit')->name('printer.edit');

    Route::put('/admin/printers/{printer}', 'PrintersController@update')->name('printer.update');

    Route::put('/comment/{comment}', 'CommentsController@update')->name('comment.update');

    Route::put('/requests/{request}', 'RequestsController@update')->name('requests.update');
});

Route::group(['middleware' => ['auth']], function () {
    Route::post('/requests', 'RequestsController@store')->name('requests.store');

    Route::post('/requests/{request}/delete', 'RequestsController@destroy')->name('requests.destroy');

    Route::get('/requests/create', 'RequestsController@create')->name('requests.create');

    Route::get('/requests/{request}', 'RequestsController@show')->name('requests.show');

    Route::get('/requests/{request}/edit', 'RequestsController@edit')->name('requests.edit');

    Route::put('/requests/{request}', 'RequestsController@update')->name('requests.update');

    Route::put('/requests/{request}/evaluate', 'RequestsController@evaluate')->name('requests.evaluate');

    Route::post('/logout', 'Auth\LoginController@destroy')->name('logout');

    Route::post('/comment', 'CommentsController@store')->name('comment.store');

    Route::put('/reset/{user}', 'Auth\ResetPasswordController@update')->name('reset.update');

    Route::get('/users/{user}/edit', 'UsersController@edit')->name('user.edit');

    Route::put('/user/{user}', 'UsersController@update')->name('user.update');

    Route::get('thumbnails/request/{request}', function ($image) {
    return Image::make(storage_path('app/public/thumbnails/'.$image))->response();
})->name('thumbnail.show');

});
