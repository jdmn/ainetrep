<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'admin' => $faker->admin = false,
        'blocked' => false,
        'print_evals' => 0,
        'print_counts' => 0,
        'department_id' => 1,
    ];
});

$factory->define(App\Departaments::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,

    ];
});

$factory->define(App\Requests::class, function (Faker\Generator $faker) {
    return [
        'owner_id' => $faker,
        'status' => $faker->numberBetween(0, 3),
        'open_date' => $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get()),
        'due_date' => null,
        'description' => null,
        'quantity' => $faker->randomDigitNotNull,
        'colored' => $faker->numberBetween(0, 1),
        'stapled' => $faker->numberBetween(0, 1),
        'paper_size' => $faker->numberBetween(0, 3),
        'paper_type' => $faker->numberBetween(0, 3),
        'file' => $faker->file($sourceDir = '/tmp', $targetDir = '/tmp'),
        'printer_id' => $faker->numberBetween(0, 1),
        'closed_date' => $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get()),
        'refused_reason' => null,
        'satisfaction_grade' => null,
    ];
});
